#
# refind/Makefile
# Build control file for the rEFInd boot menu
#

SRCDIR	= .
TARGET	= tiny.efi
OBJS    = main.o

all: $(TARGET)

include $(SRCDIR)/Make.common

$(TARGET): $(SHLIB_TARGET)
	$(OBJCOPY) -j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel \
		   -j .rela -j .reloc --target=$(FORMAT) $< $@
	chmod a-x $(TARGET)

# EOF
# DO NOT DELETE
