#ifndef __EFI_ESN_H
#define __EFI_ESN_H

#include <efi.h>
#include <efilib.h>

#define EFI_SIMPLE_NETWORK_GUID                                 \
   { 0xa19832b9, 0xac25, 0x11d3,                                \
      { 0x9a, 0x2d, 0x00, 0x90, 0x27, 0x3f, 0xc1, 0x4d }        \
   }

typedef struct mac_addr
{
   UINT8 addr[32];
}efi_mac_t;

typedef struct
{
  UINT64 RxTotalFrames;
  UINT64 RxGoodFrames;
  UINT64 RxUndersizeFrames;
  UINT64 RxOversizeFrames;
  UINT64 RxDroppedFrames;
  UINT64 RxUnicastFrames;
  UINT64 RxBroadcastFrames;
  UINT64 RxMulticastFrames;
  UINT64 RxCrcErrorFrames;
  UINT64 RxTotalBytes;
  UINT64 TxTotalFrames;
  UINT64 TxGoodFrames;
  UINT64 TxUndersizeFrames;
  UINT64 TxOversizeFrames;
  UINT64 TxDroppedFrames;
  UINT64 TxUnicastFrames;
  UINT64 TxBroadcastFrames;
  UINT64 TxMulticastFrames;
  UINT64 TxCrcErrorFrames;
  UINT64 TxTotalBytes;
  UINT64 Collisions;
  UINT64 UnsupportedProtocol;
}esn_statistics_t;

typedef struct efi_simple_network_mode
{
   UINT32 state;
   UINT32 hwaddr_size;
   UINT32 media_header_size;
   UINT32 max_packet_size;
   UINT32 nvram_size;
   UINT32 nvram_access_size;
   UINT32 receive_filter_mask;
   UINT32 receive_filter_setting;
   UINT32 max_mcast_filter_count;
   UINT32 mcast_filter_count;
   efi_mac_t mcast_filter[16];
   efi_mac_t current_address;
   efi_mac_t broadcast_address;
   efi_mac_t permanent_address;
   UINT8 if_type;
   UINT8 mac_changeable;
   UINT8 multitx_supported;
   UINT8 media_present_supported;
   UINT8 media_present;
}esn_mode_t;

typedef struct efi_simple_network
{
   UINT64 revision;
   EFI_STATUS (*start) (struct efi_simple_network *this);
   EFI_STATUS (*stop) (struct efi_simple_network *this);
   //VOID (*stop) (void);
   EFI_STATUS (*initialize) (struct efi_simple_network *this,
                             UINTN extra_rx,
                             UINTN extra_tx);
   VOID (*reset) (void);
   //VOID (*shutdown) (void);
   EFI_STATUS (*shutdown) (struct efi_simple_network *this);
   VOID (*station_address) (void);
   //VOID (*statistics) (void);
   VOID (*mcastiptomac) (void);
   VOID (*nvdata) (void);

   EFI_STATUS (*statistics) (struct efi_simple_network *this,
                             BOOLEAN Reset,
                             UINTN *StatisticsSize,
                             esn_statistics_t *StatisticsTable);

   EFI_STATUS (*receive_filters) (struct efi_simple_network *this,
                                  UINT32 Enable,
                                  UINT32 Disable,
                                  BOOLEAN ResetMCastFilter,
                                  UINTN MCastFilterCnt,
                                  efi_mac_t *MCastFilter);
   EFI_STATUS (*get_status) (struct efi_simple_network *this,
                             UINT32 *int_status,
                             VOID **txbuf);
   EFI_STATUS (*transmit) (struct efi_simple_network *this,
                           UINTN header_size,
                           UINTN buffer_size,
                           VOID *buffer,
                           efi_mac_t *src_addr,
                           efi_mac_t *dest_addr,
                           UINT16 *protocol);
   EFI_STATUS (*receive) (struct efi_simple_network *this,
                          UINTN *header_size,
                          UINTN *buffer_size,
                          VOID *buffer,
                          efi_mac_t *src_addr,
                          efi_mac_t *dest_addr,
                          UINT16 *protocol);
   VOID (*waitforpacket) (void);
   struct efi_simple_network_mode *mode;
}efi_simple_network_t;

#endif
