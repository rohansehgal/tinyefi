#ifndef __BOOTP_H
#define __BOOTP_H

struct in_addr
{
   uint32_t       s_addr;     /* address in network byte order */
};

struct bootp
{
   unsigned char	bp_op;		/* packet opcode type */
   unsigned char	bp_htype;	/* hardware addr type */
   unsigned char	bp_hlen;	/* hardware addr length */
   unsigned char	bp_hops;	/* gateway hops */
   unsigned int	        bp_xid;		/* transaction ID */
   unsigned short	bp_secs;	/* seconds since boot began */
   unsigned short	bp_flags;
   struct in_addr	bp_ciaddr;	/* client IP address */
   struct in_addr	bp_yiaddr;	/* 'your' IP address */
   struct in_addr	bp_siaddr;	/* server IP address */
   struct in_addr	bp_giaddr;	/* gateway IP address */
   unsigned char	bp_chaddr[16];	/* client hardware address */
   unsigned char	bp_sname[64];	/* server host name */
   unsigned char	bp_file[128];	/* boot file name */
#ifdef SUPPORT_DHCP
#define BOOTP_VENDSIZE 312
#else
#define BOOTP_VENDSIZE 64
#endif
   unsigned char	bp_vend[BOOTP_VENDSIZE]; /* vendor-specific area */
};

/*
 * UDP port numbers, server and client.
 */
#define	IPPORT_BOOTPS		67
#define	IPPORT_BOOTPC		68

#define BOOTREPLY		2
#define BOOTREQUEST		1

#endif
