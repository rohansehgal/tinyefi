#include <efi.h>
#include <efilib.h>
#include "efi_esn.h"
#include "refit_call_wrapper.h"

#define EFI_OPEN_PROTOCOL_GET_PROTOCOL		0x00000002
#define ATTR_BASIC (EFI_LIGHTGRAY | EFI_BACKGROUND_BLACK)
#define ATTR_ERROR (EFI_YELLOW | EFI_BACKGROUND_BLACK)

/* Debug Macros */
#define WIDE_STRING(val) L###val
#define logvar_hex(x) Print(L"\n%s = 0x%x\n", WIDE_STRING(x), x)
#define logvar_int(x) Print(L"\n%s = %d\n", WIDE_STRING(x), x)
#define logvar_nl() Print(L"\n")
#define logvar_long(x) Print(L"\n%s = 0x%lx\r", WIDE_STRING(x), x)

#define INF_LOOP() while(1)
#define STATUS_DEVICE_ERROR 0x8000000000000007
#define STATUS_SUCCESS 0x8000000000000000
#define STATUS_DEVICE_NOT_READY 0x8000000000000006

typedef struct
{
   UINTN dev_error;
   UINTN not_ready;
   UINTN success;
   UINTN others;
}local_stats_t;

EFI_HANDLE       SelfImageHandle;
EFI_LOADED_IMAGE *SelfLoadedImage;
EFI_GUID netGuid = EFI_SIMPLE_NETWORK_GUID;
SIMPLE_TEXT_OUTPUT_INTERFACE *conout;
EFI_SYSTEM_TABLE* ST;
EFI_BOOT_SERVICES* BS;
EFI_RUNTIME_SERVICES* RT;

char sendpacket[342] =
{
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x26,
   0x08, 0x05, 0x4d, 0x7e, 0x08, 0x00, 0x45, 0x00,
   0x01, 0x48, 0x24, 0x01, 0x00, 0x00, 0xff, 0x11,
   0x96, 0xa4, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff,
   0xff, 0xff, 0x00, 0x44, 0x00, 0x43, 0x01, 0x34,
   0xf6, 0x0e, 0x01, 0x01, 0x06, 0x00, 0x54, 0x4e,
   0x2a, 0xfc, 0x2a, 0xfc, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x26,
   0x08, 0x05, 0x4d, 0x7e
};


void print_str(IN CHAR16 *what)
{
   uefi_call_wrapper(conout->OutputString, 2, conout, what);
}

BOOLEAN CheckFatalError(IN EFI_STATUS Status, IN CHAR16 *where)
{

   if (!EFI_ERROR(Status))
      return FALSE;

   refit_call2_wrapper(ST->ConOut->SetAttribute, ST->ConOut, ATTR_ERROR);
   Print(L"Fatal Error: %s\n", where);
   refit_call2_wrapper(ST->ConOut->SetAttribute, ST->ConOut, ATTR_BASIC);
   return TRUE;
}

/**
 * @brief this function tries to open the simple net protocol
 * on a passed in device.
 *
 * @param devHandle
 * @param controllerHandle
 *
 * @return
 */
VOID* efi_net_open_protocol(EFI_HANDLE devHandle,
                      EFI_HANDLE controllerHandle)
{
   EFI_STATUS Status;
   VOID* interface;

   Print(L"devHandle 0x%lx,\nagentHandle 0x%lx,\nattr 0x%x",
         (UINT64)devHandle,
         (UINT64)controllerHandle,
         EFI_OPEN_PROTOCOL_GET_PROTOCOL);

   Status = refit_call6_wrapper(BS->OpenProtocol,
                                devHandle,
                                &netGuid,
                                &interface,
                                controllerHandle,
                                0,
                                EFI_OPEN_PROTOCOL_GET_PROTOCOL);

   if (CheckFatalError(Status, L"while opening protocol"))
   {
      return 0;
   }
   else
   {
      Print(L"\nefi_net_open_protocol Interface is at %lx\n", (UINT64)interface);
      return interface;
   }
}

/**
 * @brief given a guid and a buffer (to store the device list in)
 * find all devices that support this guid.
 *
 * @param guid
 * @param buf
 * @param buf_size
 *
 * @return
 */
UINTN efi_get_dev_from_guid(VOID* guid, VOID* buf, UINTN buf_size)
{
   EFI_STATUS Status;
   UINTN size_sent_in = buf_size;

   Status = refit_call5_wrapper(BS->LocateHandle,
                                2,
                                guid,
                                0,
                                &size_sent_in,
                                buf);

   if (CheckFatalError(Status, L"while getting a GUID handle"))
   {
      return 0;
   }
   return size_sent_in;
}

/**
 * @brief get the loaded image.
 *
 * @param ImageHandle
 *
 * @return
 */
EFI_STATUS efi_init_lib(IN EFI_HANDLE ImageHandle)
{
   EFI_STATUS  Status;

   SelfImageHandle = ImageHandle;
   Status = refit_call3_wrapper(BS->HandleProtocol,
                                SelfImageHandle,
                                &LoadedImageProtocol,
                                (VOID **) &SelfLoadedImage);
   if (CheckFatalError(Status, L"while getting a LoadedImageProtocol handle"))
      return EFI_LOAD_ERROR;

   return EFI_SUCCESS;
}

/**
 * @brief populate the table pointers.
 *
 * @param ImageHandle
 * @param SystemTable
 *
 * @return
 */
EFI_STATUS efi_init_lib_globals(IN EFI_HANDLE ImageHandle,
                                IN EFI_SYSTEM_TABLE *SystemTable)
{
   ST = SystemTable;
   BS = SystemTable->BootServices;
   RT = SystemTable->RuntimeServices;
   conout = SystemTable->ConOut;
   return EFI_SUCCESS;
}


EFI_STATUS esn_start_interface(efi_simple_network_t* esn)
{
   EFI_STATUS Status = refit_call1_wrapper(esn->start, esn);
   if (CheckFatalError(Status, L"while starting interface"))
   {
   }

   return Status;
}

EFI_STATUS esn_init_interface(efi_simple_network_t *esn)
{
   EFI_STATUS Status = refit_call3_wrapper(esn->initialize, esn, 0, 0);
   if (CheckFatalError(Status, L"while initializing interface"))
   {
   }
   return Status;
}

EFI_STATUS esn_shutdown_interface(efi_simple_network_t *esn)
{
   EFI_STATUS Status = refit_call1_wrapper(esn->shutdown, esn);
   if (CheckFatalError(Status, L"while shutting down interface"))
   {
   }
   return Status;
}

EFI_STATUS esn_stop_interface(efi_simple_network_t *esn)
{
   EFI_STATUS Status = refit_call1_wrapper(esn->stop, esn);
   if (CheckFatalError(Status, L"while stopping interface"))
   {
   }
   return Status;
}

EFI_STATUS efi_disable_watchdog()
{
   //disables the watchdog timer
   return refit_call4_wrapper(BS->SetWatchdogTimer, 0x0, 0x0, 0x0, NULL);
}

char* get_64kb_buffer()
{
    char* buf = AllocatePool(128*4096);
    buf += 64*4096;
    buf = (char *)((UINTN)buf & (~0xffff));
    return buf;
}

/**
 * @brief gets the status
 *
 * @param int_status - interrupt status, if NULL, don't read, reading clears it
 * @param txbuf - get the last txbuf, if NULL, don't read, if returned value is
 * NULL, transmission in progress.
 *
 * @return
 */
static EFI_STATUS esn_get_status(efi_simple_network_t* esn,UINT32* int_status, VOID** txbuf)
{
   EFI_STATUS Status;

   Status = refit_call3_wrapper(esn->get_status, esn, int_status, txbuf);

   if (CheckFatalError(Status, L"while checking status"))
   {
   }

   return Status;
}

static EFI_STATUS esn_get_statistics(efi_simple_network_t* esn,
                                     esn_statistics_t *stats,
                                     UINTN *stats_size)
{
   EFI_STATUS Status = refit_call4_wrapper(esn->statistics, esn, FALSE,
                                           stats_size, stats);

   logvar_long(Status);
   if (CheckFatalError(Status, L"while statistics"))
   {
   }

   return Status;
}

/**
 * @brief the efi execution starts here.
 *
 * @param ImageHandle
 * @param SystemTable
 *
 * @return
 */

EFI_STATUS esn_receive(efi_simple_network_t *esn, char *recv_buffer,
                       UINTN *recv_size, efi_mac_t *src, efi_mac_t *dest)
{
   EFI_STATUS Status = refit_call7_wrapper(esn->receive, esn, NULL, recv_size,
                                           recv_buffer, src, dest, NULL);
   return Status;
}

EFI_STATUS esn_transmit(efi_simple_network_t *esn, char *packet,
                        UINTN pack_len, efi_mac_t *src, efi_mac_t *dest)
{
   EFI_STATUS Status = refit_call7_wrapper(esn->transmit, esn, 0, pack_len,
                                           packet, src, dest, NULL);

   logvar_long(Status);
   if (CheckFatalError(Status, L"while transmitting"))
   {
   }

   return Status;
}

void track_status(EFI_STATUS Status, local_stats_t *local_stats)
{
   switch(Status)
   {
   case EFI_DEVICE_ERROR:
      local_stats->dev_error++;
      break;

   case EFI_NOT_READY:
      local_stats->not_ready++;
      break;

   case EFI_SUCCESS:
      local_stats->success++;
      break;

   default:
      local_stats->others++;
      break;
   }
}

VOID esn_print_mode(efi_simple_network_t* esn)
{
   esn_mode_t *mode = esn->mode;
   Print(L"mode.status = %d\nmode.current_address%lx\n"\
         L"mode.max_packet_size = %d\nmode.iftype = %2x", 
         mode->state
         , *((UINTN*)&(mode->current_address))
         , mode->max_packet_size
         , mode->if_type); 
}

EFI_STATUS
EFIAPI
efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
   void *buf;
   EFI_STATUS Status;
   UINT32 size = sizeof(EFI_HANDLE)*1024;

   Status = efi_init_lib_globals(ImageHandle, SystemTable);
   Status = efi_init_lib(ImageHandle);

   buf = AllocatePool(sizeof(EFI_HANDLE)*1024);

   if (EFI_ERROR(Status))
      return Status;

   Status = efi_disable_watchdog();
   size = efi_get_dev_from_guid(&netGuid, buf, size);

   if (size == 0)
   {
      Print(L"\nCouldn't find any device supporting EFI SIMPLE NETWORK PROTOCOL\n");
      return EFI_DEVICE_ERROR;
   }

   EFI_HANDLE* h = buf;
   int i, num_devs = size / sizeof(EFI_HANDLE);

   logvar_nl();
   for (i = 0; i < num_devs; i++)
   {
      int j;
      VOID* tx = NULL;
      UINT32 int_status;
      char stack_buf[1500];
      UINTN recv_size = 1500;
      char* recvpacket = stack_buf;
      Print(L"stack_buf %lx\n", (UINTN)recvpacket);
      //try 64kb buffer -- comment this out if unneeded
      recvpacket = get_64kb_buffer();
      Print(L"64_buf %lx\n", (UINTN)recvpacket);

      esn_statistics_t stats = {0};
      local_stats_t local_stats = {0};
      UINTN stats_size = sizeof(esn_statistics_t);
      efi_simple_network_t* esn = efi_net_open_protocol(*h, ImageHandle);

      Status = esn_start_interface(esn);
      Status = esn_init_interface(esn);
      Status = esn_get_status(esn, &int_status, tx);
      Status = esn_transmit(esn, sendpacket, sizeof(sendpacket), NULL, NULL);
      esn_print_mode(esn);
      Status = esn_get_statistics(esn, &stats, &stats_size);

      Print(L"For Device %d\n", i);
      for(j = 0; j < 80000; j++)
      {
         Status = esn_receive(esn, recvpacket, &recv_size, NULL, NULL);
         track_status(Status, &local_stats);
         Print(L"Dev_Err = %d, Not_Ready = %d, Success = %d, Others = %d\r",
               local_stats.dev_error, local_stats.not_ready,
               local_stats.success, local_stats.others);
      }
      logvar_nl();
      Status = esn_shutdown_interface(esn);
      Status = esn_stop_interface(esn);
   }
   return EFI_SUCCESS;
}
